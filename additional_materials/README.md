# additional_materials

This folder contains plots that describe the ESHM20 data
and compare these results to the ESHM13 model.

- `source_model_plots/`
  - `attributes/` contains a subdirectory tree of plots that show the regional
  variation in all attributes for selected shapefiles.  All tecto and asm
  shapefiles are represetned, as is the smoothed seismicity model (ssm)
- `comparison_plots/`
  - `ESHM20vsESHM13/`
      - `haz_curves_comparison_plots/` contain plots that compare the hazard
      curves between this ESHM20 model, and the previous EHSM13 model.
      - `haz_spectra_comparison_plots/RP475yrs/` contain plots that compare the hazard
      spectra between this ESHM20 model, and the previous EHSM13 model at a 
      return period of 475 years.
- `ground_motion_trellis_plots`

### Reference and Citing:
Please use the following reference for citing this repository:
#### Danciu L., Nandan S., Reyes C., Basili R., Weatherill G., Beauval C., Rovida A., Vilanova S., Sesetyan K., Bard P-Y., Cotton F., Wiemer S., Giardini D. (2021) - The 2020 update of the European Seismic Hazard Model: Model Overview. EFEHR Technical Report 001, v1.0.0, <!--[![DOI](https://doi.org/10.12686/a15)](https://doi.org/10.12686/a15)-->https://doi.org/10.12686/a15

#### The use of any of these files without proper citation violates the CC-BY 4.0 license and is strictly prohibited!!


---------------------------------------------------------------------------------------
### Disclaimer:

The findings, comments, statements or recommendations expressed herein are exclusively
of the author(s) and do not necessarily reflect the views and policies of the institutions listed here: i.e. Swiss Seismological Service, ETH Zurich, Istituto
Nazionale di Geofisica e Vulcanologia (INGV), German Research Centre for Geociences (GFZ), Institut des Sciences de la Terre (ISTerre), Instituto Superior
Tecnico (IST), Bogazici University, Kandilli Observatory and Earthquake Research Institute, Department of Earthquake Engineering, the EFEHR Consortium
or the European Union. 

The authors of ESHM20 have tried to make the information in this product as accurate as possible. However, they do not guarantee that
the information herein is totally accurate or complete. Therefore, you should not solely rely on this information when making a commercial decision. 

Users of information provided herein assume all liability arising from such use. While undertaking to provide practical and accurate information, the authors assume no liability for, nor express or imply any warranty with regard to the information contained hereafter.

Licence: creativecommons cc-by 4.0
https://creativecommons.org/licenses/by/4.0/

You are free to:
Share — copy and redistribute the material in any medium or format for any purpose, even commercially.
Adapt — remix, transform, and build upon the material for any purpose, even commercially.
The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:
###### <strong><font color="red">Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.</font></strong>

No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

##### Notices:
You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation .
No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.

For more please refer to:
https://creativecommons.org/licenses/by/4.0/deed.en#ref-indicate-changes


**Contact Us**
If you have any questions or feedback on the data included in this repository, please send it via email to 'efehr.hazard[at]sed.ethz.ch'.

