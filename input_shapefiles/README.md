# input_shapefiles

[![Static Badge](https://img.shields.io/badge/DOI-10.12686%2Feshm20--main--datasets-blue)](https://doi.org/10.12686/eshm20-main-datasets) [![License: CC BY 4.0](https://img.shields.io/badge/License-CC_BY_4.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/)


This folder contains the ESRI shapefiles that represent the
data that was used in the development of the 2020 update of the 
European Seismic Hazard Model - ESHM20.

### References and Citation:
Please use this citation when the following datasets are used:

*Danciu, L., Nandan, S., Reyes, C., Basili, R., Weatherill, G., Beauval, C., Rovida, A., Vilanova, S., Sesetyan, K., Bard, P.-Y., Cotton, F., Wiemer, S., & Giardini, D. (2021). Main Datasets of the 2020 Update of the European Seismic Hazard Model (ESHM20) [Data set]. EFEHR European Facilities of Earthquake Hazard and Risk. https://doi.org/10.12686/ESHM20-MAIN-DATASETS*


> :exclamation: The use of any of these files without proper citation violates the CC-BY 4.0 license and is strictly prohibited :exclamation:



#### The use of any of these files without proper citation violates the CC-BY 4.0 license and is strictly prohibited!!


### A brief description of each follows:

`eshm20_input_a_unified_eq_catalogue/` contains the unified earthquake catalog, 
which contains all the events that were used in creating this model.  

- `eshm20_input_b_completeness_superzones/` contains the zones that were used to
determine the magnitude of completeness M<sub>c</sub> through time, at an annual granularity. 


- `eshm20_input_c_tecto_zones/` contains the tectonic source zones for the
shallow crust model


- `eshm20_input_d_asm_shallow_crust/` contains the area source zones for the
shallow crust model


- `ehsm20_input_e_fs_model/` contains the fault models


- `eshm20_input_f_smoothed_seismicity/` contains the smoothed seismicity model


- `eshm20_input_g_calc_regions_nrml_filter/` contains polygons that define regions
used to split the smoothed seismicity grid for the calculations. These regions
are the iceland, main, and swislands that are referred to in the _oq_computational_
section below


- `eshm20_input_h_simple_individual_buffer/` contains a buffer that surrounds the
fault models, used to determine which smoothed-seismicity grid-points are impacted
by each fault


- `eshm20_input_i_tecto_iceland/` contains the tectonic source zones for the iceland model


- `eshm20_input_j_asz_iceland/` contains the area source zones for he iceland model


- `eshm20_input_k_tecto_deep/` contains the tectonic source zones for the deep model


- `eshm20_input_m_asz_deep/` contains the area source zones for the deep model


- `eshm20_input_n_asz_volcanic/` contains the area source zones for the shallow volcanic model



---------------------------------------------------------------------------------------
### Disclaimer:

The findings, comments, statements or recommendations expressed herein are exclusively
of the author(s) and do not necessarily reflect the views and policies of the institutions listed here: i.e. Swiss Seismological Service, ETH Zurich, Istituto
Nazionale di Geofisica e Vulcanologia (INGV), German Research Centre for Geociences (GFZ), Institut des Sciences de la Terre (ISTerre), Instituto Superior
Tecnico (IST), Bogazici University, Kandilli Observatory and Earthquake Research Institute, Department of Earthquake Engineering, the EFEHR Consortium
or the European Union. 

The authors of ESHM20 have tried to make the information in this product as accurate as possible. However, they do not guarantee that
the information herein is totally accurate or complete. Therefore, you should not solely rely on this information when making a commercial decision. 

Users of information provided herein assume all liability arising from such use. While undertaking to provide practical and accurate information, the authors assume no liability for, nor express or imply any warranty with regard to the information contained hereafter.

Licence: creativecommons cc-by 4.0
https://creativecommons.org/licenses/by/4.0/

You are free to:
Share — copy and redistribute the material in any medium or format for any purpose, even commercially.
Adapt — remix, transform, and build upon the material for any purpose, even commercially.
The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:
###### <strong><font color="red">Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.</font></strong>

No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

##### Notices:
You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation .
No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.

For more please refer to:
https://creativecommons.org/licenses/by/4.0/deed.en#ref-indicate-changes


**Contact Us**
If you have any questions or feedback on the data included in this repository, please send it via email to 'efehr.hazard[at]sed.ethz.ch'.
