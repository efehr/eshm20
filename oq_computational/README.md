# oq_computational

 [![Static Badge](https://img.shields.io/badge/DOI-10.12686%2Feshm20--oq--input-blue)](https://doi.org/10.12686/eshm20-oq-input) [![License: CC BY 4.0](https://img.shields.io/badge/License-CC_BY_4.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/)

This repository folder contains the configuration files and openquake input nrml (.xml) files used to run ESHM20 calculation.

### References and Citation:
Please use this citation when the OpenQuake input files are used:

*Danciu, L., Nandan, S., Reyes, C., Wiemer, S., & Giardini, D. (2021). OpenQuake Input Files for the 2020 Update of the European Seismic Hazard Model (ESHM20) [Data set]. EFEHR European Facilities of Earthquake Hazard and Risk. https://doi.org/10.12686/ESHM20-OQ-INPUT*

> :exclamation: The use of any of these files without proper citation violates the CC-BY 4.0 license and is strictly prohibited :exclamation:

#### The use of any of these files without proper citation violates the CC-BY 4.0 license and is strictly prohibited!

Main folders and files:

  - `oq_configuration_eshm20_v1e_region_iceland/` contains the input files for Iceland
    - `source_models/`
      - `asm_v12e/` contains correlated area-source model input files.  Here are
      9 truncated Gutenberg-Richter models, resulting from a combination of 3
      different max-magnitudes (low,mid,upp) with 3 
      aGR/bGR combinations (lo,mid,hi), plus 3 Pareto models, each with a different
      corner magnitude (low,mid,upp).
      - `fsm_v12e/` contains the 9 branches of the fault model, which combine
      truncated Gutenberg-Richter models containing three slip rates (SRA, SRL, SRU)
      with three max-magnitudes (MA, ML, MU).
      
      - `ssm_v12e/` contains the smoothed seismicity model
    - `config_eshm20_v12e_iceland.ini`
    - `gmpe_logic_tree_iceland_5branch.xml`
    - `source_model_logic_tree_eshm20_model_iceland_v12e.xml`
    

  - `oq_configuration_eshm20_v1e_region_main/` contains the configuration and input files for mainland europe, including the British isles
    - `source_models/`
      - `asm_v12e/` contains correlated area-source model input files.  Here are
      9 truncated Gutenberg-Richter models, resulting from a combination of 3
      different max-magnitudes (low,mid,upp) with 3 
      aGR/bGR combinations (lo,mid,hi), plus 3 Pareto models, each with a different
      corner magnitude (low,mid,upp).
      - `deep_v12e/`contains 12 correlated models for the deep regions (see _asm_v12e_, above)
      - `fsm_v09/` contains the 9 branches of the fault model, which combine
      truncated Gutenberg-Richter models containing three slip rates (SRA, SRL, SRU)
      with three max-magnitudes (MA, ML, MU).
      - `interface_v12b/` contains the 4 interface input files
      - `ssm_v09/` contains the smoothed seismicity model
      - `volcanic_v12e/` contains 12 correlated models (see _asm_v12e_, above) for
      volcanic regions
  
    - `config_eshm20_v12e_main_region.ini`
    - `eshm20_site_model_v06d.csv`
    - `gmpe_logic_tree_5br.xml`
    - `source_model_logic_tree_eshm20_model_v12e.xml`
    

  - `oq_configuration_eshm20_v1e_region_swislands/` contains the input files for the islands
south and west of spain. 
    - `source_models/`
      - `asm_v12e/` contains correlated area-source model input files.  Here are
      9 truncated Gutenberg-Richter models, resulting from a combination of 3
      different max-magnitudes (low,mid,upp) with 3 
      aGR/bGR combinations (lo,mid,hi), plus 3 Pareto models, each with a different
      corner magnitude (low,mid,upp).
      - `fsm_v12e/` contains the 9 branches of the fault model, which combine
      truncated Gutenberg-Richter models containing three slip rates (SRA, SRL, SRU)
      with three max-magnitudes (MA, ML, MU).
      - `ssm_v12e/` contains the smoothed seismicity model
    - `config_eshm20_v12e_oceanic.ini`
    - `gmpe_complete_logic_tree_5br.xml`
    - `oceanic_site_model_5km.xml`
    - `source_model_logic_tree_eshm20_oceanic_v12e.xml`


---------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------
### Disclaimer:

The findings, comments, statements or recommendations expressed herein are exclusively
of the author(s) and do not necessarily reflect the views and policies of the institutions listed here: i.e. Swiss Seismological Service, ETH Zurich, Istituto
Nazionale di Geofisica e Vulcanologia (INGV), German Research Centre for Geociences (GFZ), Institut des Sciences de la Terre (ISTerre), Instituto Superior
Tecnico (IST), Bogazici University, Kandilli Observatory and Earthquake Research Institute, Department of Earthquake Engineering, the EFEHR Consortium
or the European Union. 

The authors of ESHM20 have tried to make the information in this product as accurate as possible. However, they do not guarantee that
the information herein is totally accurate or complete. Therefore, you should not solely rely on this information when making a commercial decision. 

Users of information provided herein assume all liability arising from such use. While undertaking to provide practical and accurate information, the authors assume no liability for, nor express or imply any warranty with regard to the information contained hereafter.

Licence: creativecommons cc-by 4.0
https://creativecommons.org/licenses/by/4.0/

You are free to:
Share — copy and redistribute the material in any medium or format for any purpose, even commercially.
Adapt — remix, transform, and build upon the material for any purpose, even commercially.
The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:
###### <strong><font color="red">Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.</font></strong>

No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

##### Notices:
You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation .
No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.

For more please refer to:
https://creativecommons.org/licenses/by/4.0/deed.en#ref-indicate-changes


**Contact Us**
If you have any questions or feedback on the data included in this repository, please send it via email to 'efehr.hazard[at]sed.ethz.ch'.
